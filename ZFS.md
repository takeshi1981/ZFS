# :turtle: ZFS

## 1. ZFSの管理に必要なコマンド

- **zfsコマンド**  

  - ストレージプール上にファイルシステムの作成、マウント
  - スナップショットやクローンの作成
  - ファイルシステムのバックアップ、リストア

- **zpoolコマンド**

  - 物理ディスクからストレージプールの作成
  - ディスクの冗長構成（RAID）を設定
  - データ整合性チェック（ディスククラブ）

## 2. ZFSの信頼性・可用性

- **コピーオンライト**  
・・・ファイルへの書き込み時に元のデータを上書きしない仕組み

- **トランザクションファイルシステム**  
・・・データの更新時に整合性を確保する仕組み

- **チェックサム**  
・・・End-to-Endのチェックサムによる信頼性の高いデータ保護

- **スクラブ**  
・・・ストレージプール内の全データの完全性をチェックする機能

- **RAID-Z** **RAID-Z2**  
・・・物理ディスクの損傷時にデータを守るZFS特有のRAID構成

## 3. Install ZFS Package(CentOS8)

- ZFSリポジトリ登録

      dnf install https://zfsonlinux.org/epel/zfs-release.<dist>.noarch.rpm
      ※<dist>
      ・・・https://openzfs.github.io/openzfs-docs/Getting%20Started/RHEL%20and%20CentOS.html

      gpg --quiet --with-fingerprint /etc/pki/rpm-gpg/RPM-GPG-KEY-zfsonlinux

      sudo dnf install epel-release

- ZFSインストール

      dnf -y update
      dnf -y install kernel-devel zfs

- モジュールロード

      modprobe zfs
      dmesg | tail -5

## 4. ZFSプール

- プール作成(RAID0)

      zpool create <pool-name> /dev/sda /dev/sdb

- プール作成(RAID1)

      zpool create <pool-name> mirror /dev/sdc /dev/sdd

- プール作成(RAID5)

      zpool create <pool-name> raidz /dev/sde /dev/sdf /dev/sdg

- プール作成(RAID10)

      zpool create <pool-name> mirror /dev/sdh /dev/sdi mirror /dev/sdj /dev/sdk

- プールリスト確認

      zpool list

- プール状態確認

      zpool status

- プールの削除

      zpool destroy <pool-name>

## 5. ZFSファイルシステム

- ファイルシステム作成

      zfs create <zpool-name>/<mount-point>

## 6. [Appendix] FOG Project

- url : <https://github.com/FOGProject/fogproject>
